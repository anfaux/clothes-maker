import "./App.css";
import { Provider } from "react-redux";
import store from "./state/store";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { Home } from "./pages";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/" render={() => <Redirect to="/" />} />
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
