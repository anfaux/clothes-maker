import React from "react";
const Iconify = ({ icon, size = 26, onClick }) => (
  <img
    onClick={onClick}
    alt={icon}
    src={`iconify/png/32px/${icon}@32px.png`}
    style={{ filter: "brightness(0) invert(1)", width: size, height: size }}
  />
);
export default Iconify;
