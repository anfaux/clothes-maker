import React, { useState } from "react";
import WatchForOutsideClick from "components/WatchForOutsideClick";
import Iconify from "components/Iconify";

function ModelSelector({ models, onSelect, style }) {
  const [hover, setHover] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const styles = {
    ...style,
    container: {
      backgroundColor: "rgb(39 39 39)",
      width: "10rem",
      position: "fixed",
      top: 0,
      bottom: 0,
      zIndex: 1,
      boxShadow: "-1px 5px 5px black",
    },
  };

  if (!isOpen) {
    return (
      <div style={{ ...styles.container, width: "2rem", paddingTop: ".5rem" }}>
        <div onClick={() => setIsOpen(true)}>
          <Iconify icon="documents-folder" />
        </div>
        <div onClick={() => console.log("Info")}>
          <Iconify icon="documents-document-favorites" />
        </div>
      </div>
    );
  } else {
    return (
      <WatchForOutsideClick onOutsideClick={() => setIsOpen(false)}>
        <div style={styles.container}>
          {models.map((model, idx) => (
            <div
              key={idx}
              style={{
                margin: "1rem",
                boxShadow:
                  hover === idx
                    ? `inset 0px 0px 0px 2px ${style.theme_highlight}`
                    : "",
                padding: ".5rem",
                fontSize: ".8rem",
                fontWeight: "700",
              }}
              onClick={() => {
                if (
                  window.confirm(
                    "Do you want to load a new model? This will reset the tool and you will lose unsaved work."
                  )
                )
                  onSelect(model);
              }}
            >
              <div
                onMouseOver={() => setHover(idx)}
                onMouseOut={() => setHover(null)}
              >
                <img
                  alt=""
                  src={`models/${model.folder}/preview.png`}
                  style={{ width: 100, height: 100, borderRadius: "0.2rem" }}
                />
              </div>
              <div>{model.name}</div>
            </div>
          ))}
        </div>
      </WatchForOutsideClick>
    );
  }
}

export default ModelSelector;
