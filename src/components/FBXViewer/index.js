import React, { useState, useRef, useEffect } from "react";
import fbxViewer from "./fbxViewer";

const FBXViewer = ({
  fbx,
  material,
  uv,
  style,
  showStats = false,
  size,
  onClick,
  isActive,
}) => {
  const threeContainer = useRef(null);
  const threeRenderer = useRef(null);
  const applyBlob = useRef(null);
  let stats = useRef(null);

  const { width, height } = size;
  let [currentModel, setCurrentModel] = useState(null);
  let [isLoaded, setIsLoaded] = useState(false);
  let [item, setItem] = useState();
  let config = useRef({});

  const styles = {
    ...style,
    theme_editor: {
      ...style.theme_editor,
      width,
      height,
      background: "radial-gradient(#444444, #000000)",
    },
    theme_panel: {
      ...style.theme_panel,
      width,
      height,
      boxShadow: isActive
        ? `inset 0px 0px 0px 1px ${style.theme_highlight}`
        : "",
    },
  };

  useEffect(() => {
    if (fbx !== null) {
      if (currentModel !== fbx) {
        fbxViewer.sceneInit(
          size,
          threeRenderer.current,
          threeContainer.current,
          showStats ? stats : null,
          ({ renderer, scene, camera }) => {
            fbxViewer.sceneSetup(scene);
            fbxViewer.lightScene(scene);
            const controls = fbxViewer.enableControls(camera, renderer);

            fbxViewer.loadFbx(
              scene,
              fbx,
              material,
              (item) => {
                applyBlob.current = (blob) =>
                  fbxViewer.applyBlobAsTexture(
                    renderer,
                    blob,
                    item,
                    scene,
                    camera
                  );
                config.current = {
                  object: item,
                  scene,
                  camera,
                  renderer,
                  controls,
                };
                setItem(item);
                setCurrentModel(fbx);
                const animate = () => {
                  requestAnimationFrame(animate);
                  renderer.render(scene, camera);
                  if (stats.current) stats.current.update();
                };
                renderer.render(scene, camera);
                animate();
                setIsLoaded(true);
              },
              () => setIsLoaded(false),
              (config) => {
                //console.log("Done");
              }
            );
          }
        );
      } else {
        if (uv) {
          if (typeof applyBlob.current === "function") applyBlob.current(uv);
        } else {
          fbxViewer.swapMaterial(material, item);
        }
      }
    }
  }, [
    threeContainer,
    size,
    threeRenderer,
    fbx,
    material,
    uv,
    item,
    showStats,
    currentModel,
    isLoaded,
  ]);

  if (config.current)
    fbxViewer.fitCameraToObject({
      camera: config.current.camera,
      selection: config.current.object,
      controls: config.current.controls,
    });
  return (
    <div
      style={styles.theme_panel}
      onMouseDown={onClick}
      onTouchStart={onClick}
    >
      <div style={styles.theme_editor}>
        {!isLoaded && (
          <div
            style={{
              border: "1px solid #3700b3",
              width,
              height,
              display: "flex",
              background: "black",
              color: "white",
              fontWeight: 900,
            }}
          >
            <div style={{ margin: "auto" }}>Loading...</div>
          </div>
        )}
        <div
          style={{ display: isLoaded ? "block" : "none" }}
          ref={threeContainer}
        />
      </div>
    </div>
  );
};

export default FBXViewer;
