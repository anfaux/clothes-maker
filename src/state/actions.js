const action = (type, payload = {}) => {
  return { type, payload };
};

export const SAMPLE_ACTION = "SAMPLE_ACTION";
export const doSampleAction = (payload) => action(SAMPLE_ACTION, payload);

export const USER_LOGIN = "USER_LOGIN";
export const USER_LOGOUT = "USER_LOGOUT";
export const userLogin = (payload) => action(USER_LOGIN, payload);
export const userLogout = () => action(USER_LOGOUT);

export const UMGR_GET_USERS = "UMGR_GET_USERS";
export const UMGR_GET_USERS_RESPONSE = "UMGR_GET_USERS_RESPONSE";
export const umgrGetUsers = (payload) => action(UMGR_GET_USERS, payload);

export const UMGR_UPDATE_USER = "UMGR_UPDATE_USER";
export const UMGR_UPDATE_USER_RESPONSE = "UMGR_UPDATE_USER_RESPONSE";
export const umgrUpdateUser = (payload) => action(UMGR_UPDATE_USER, payload);

export const UMGR_CREATE_USER = "UMGR_CREATE_USER";
export const UMGR_CREATE_USER_RESPONSE = "UMGR_CREATE_USER_RESPONSE";
export const umgrCreateUser = (payload) => action(UMGR_CREATE_USER, payload);

export const UMGR_DELETE_USER = "UMGR_DELETE_USER";
export const UMGR_DELETE_USER_RESPONSE = "UMGR_DELETE_USER_RESPONSE";
export const umgrDeleteUser = (payload) => action(UMGR_DELETE_USER, payload);

export const UMGR_RESET_USER = "UMGR_RESET_USER";
export const umgrResetUser = (payload) => action(UMGR_RESET_USER, payload);
