import { put, select } from "redux-saga/effects";
import * as actions from "state/actions.js";

export const getUsers = function* (action) {
  const reduxState = yield select();

  const response = yield _makeRequest({
    method: "GET",
    url: `${window._env_.REACT_APP_AUTH_SERVICE}/users`,
    token: reduxState.user.auth,
  });

  let payload = {};
  if (response && response.ok) {
    const res = yield response.json();
    payload = res;
  }

  yield put({ type: actions.UMGR_GET_USERS_RESPONSE, payload });
};

export const updateUser = function* (action) {
  yield console.log(action);
};

export const deleteUser = function* (action) {
  yield console.log(action);
};

export const createUser = function* (action) {
  yield console.log(action);
};

export const resetUser = function* (action) {
  yield console.log(action);
};

const _makeRequest = function* ({ token, method, url, body }) {
  try {
    const headers = {
      Accept: "application/json, application/xml, text/plain, text/html, *.*",
      "Content-Type": "application/json; charset=utf-8",
    };
    let request = {
      method: method,
      headers: token
        ? { ...headers, Authorization: "Bearer " + token }
        : headers,
      body: body
        ? typeof body === "string"
          ? body
          : JSON.stringify(body).replace(/\\u0000/g, "")
        : null,
    };
    if (!body || method === "GET") delete request.body;
    return yield fetch(url, request);
  } catch (e) {
    console.error("FATAL: Cannot reach url!");
    return e;
  }
};
