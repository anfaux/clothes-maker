import { takeLatest, all } from "redux-saga/effects";
import * as actions from "./actions.js";
import createSagaMiddleware from "redux-saga";
import rootReducer from "./reducers";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import * as userMgr from "./sagas/userMgr.js";

function* rootSaga() {
  yield all([takeLatest(actions.UMGR_GET_USERS, userMgr.getUsers)]);
  yield all([takeLatest(actions.UMGR_UPDATE_USER, userMgr.updateUser)]);
  yield all([takeLatest(actions.UMGR_DELETE_USER, userMgr.deleteUser)]);
  yield all([takeLatest(actions.UMGR_CREATE_USER, userMgr.createUser)]);
  yield all([takeLatest(actions.UMGR_RESET_USER, userMgr.resetUser)]);
}

const sagaMiddleware = createSagaMiddleware();
const store = () => {
  let composeEnhancers = composeWithDevTools({
    stateSanitizer: (state) => state,
  });
  let store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(sagaMiddleware))
  );
  sagaMiddleware.run(rootSaga);
  return store;
};
export default store();
