import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const PrivateLink = (props) => {
  const isAuthenticated = useSelector((redux) => redux.user.isAuthenticated);
  const role = useSelector((redux) => redux.user.claims?.role);
  const isAllowed = isAuthenticated
    ? props.requires
      ? props.requires.includes(role)
        ? true
        : false
      : true
    : false;
  return (
    <div style={{ margin: "0 0 0 1rem" }}>
      {isAllowed ? (
        <Link {...props}>{props.children}</Link>
      ) : props.alt ? (
        props.alt
      ) : (
        <div></div>
      )}
    </div>
  );
};
export default PrivateLink;
