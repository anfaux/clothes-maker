import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const GuestLink = (props) => {
  const isAuthenticated = useSelector((redux) => redux.user.isAuthenticated);
  return (
    <div style={{ margin: "0 0 0 1rem" }}>
      {!isAuthenticated ? (
        <Link {...props}>{props.children}</Link>
      ) : props.alt ? (
        props.alt
      ) : (
        <div></div>
      )}
    </div>
  );
};
export default GuestLink;
